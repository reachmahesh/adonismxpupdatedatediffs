package com.virginvoyages.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.virginvoyages.dao.AdonisDao;
import com.virginvoyages.model.Adonis;

@Service
public class AdonisService {
 
	@Autowired
	private AdonisDao adonisRepository;

	public List<Adonis> getMxpID(Integer id) {
		// LocalDateTime timeStamp = adonis.getAdonis_mod_TimeStamp().toLocalDateTime();

		/* TO Do */
		// To compare the timeStamp to that of Request TimeStamp for diff in Adonis
		// timestamp

		return adonisRepository.findMxpByAdonisId(id);
	}
	
	public List<Adonis> getVxpID(Integer id) {
			return adonisRepository.findVxpByAdonisId(id);
		}

	public Adonis getByDate(Integer pin, Date adonisdate, Date mxpdate) {
		return adonisRepository.findAdonisByDate(pin, adonisdate, mxpdate);
	}

	public List<Adonis> getAdonisDiff() {
		return adonisRepository.findAdonisDiff();
	}
	
	public List<Adonis> getAdonisMxpVxpDiff(){
		return adonisRepository.findAdonisMxpVxpDiff();
	}
	
}
