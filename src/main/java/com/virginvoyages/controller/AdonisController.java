package com.virginvoyages.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.virginvoyages.model.Adonis;
import com.virginvoyages.service.AdonisService;

@RestController
public class AdonisController {

	@Autowired
	AdonisService service;

	// inject via application.properties
   // @Value("${welcome.message}") 
   // String message;
    
	private static final Logger LOGGER = LoggerFactory.getLogger(AdonisController.class);

	@GetMapping("/adonismxp/{id}")
	public ResponseEntity<Adonis> getMxpID(@PathVariable Integer id) {
		try {
			LOGGER.info("AdonisMXP Input " + id);
			List<Adonis> adonis = service.getMxpID(id);
			if (adonis.size() > 0) {
				LOGGER.info("Adonis Modified TimeStamp " + adonis.get(0).getAdonis_mod_TimeStamp());
				LOGGER.info("MXP Modified TimeStamp " + adonis.get(0).getMxp_mod_TimeStamp());
			}
			return new ResponseEntity(adonis, HttpStatus.OK);
		} catch (NoSuchElementException e) {
			LOGGER.error("Error" + e.getLocalizedMessage());
			return new ResponseEntity<Adonis>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/adonisvxp/{id}")
	public ResponseEntity<Adonis> getVxpID(@PathVariable Integer id) {
		try {
			LOGGER.info("AdonisVXP Input " + id);
			List<Adonis> adonis = service.getVxpID(id);
			if (adonis.size() > 0) {
				LOGGER.info("AdonisModified TimeStamp " + adonis.get(0).getAdonis_mod_TimeStamp());
			}
			return new ResponseEntity(adonis, HttpStatus.OK);
		} catch (NoSuchElementException e) {
			LOGGER.error("Error" + e.getLocalizedMessage());
			return new ResponseEntity<Adonis>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/adonisdiff")
	public ResponseEntity<Adonis> getAdonisDiff() {
		try {
			List<Adonis> adonis = service.getAdonisDiff();
			if (adonis.size() > 0) {
				LOGGER.info("AdonisDiff Size " + adonis.size());
			}
			return new ResponseEntity(adonis, HttpStatus.OK);
		} catch (NoSuchElementException e) {
			LOGGER.error("Error" + e.getLocalizedMessage());
			return new ResponseEntity<Adonis>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/adonismxpvxpdiff")
	public ModelAndView getAdonisMxpVxpDiff() {

		String viewName = "adonis";
		Map<String, Object> model = new HashMap<String, Object>();
		try {

			List<Adonis> resultDiff = new ArrayList<Adonis>();
			List<Adonis> adonisVxp = service.getAdonisMxpVxpDiff();

			RestTemplate restTemplate = new RestTemplate();
			LOGGER.info("==== RESTful  START =======");
			Adonis[] mxpresponse = restTemplate.getForObject("http://10.101.224.114/API/MXP_Virgin.exe/crew",
					Adonis[].class);

			List<Adonis> adonisMxp = new LinkedList<Adonis>(Arrays.asList(mxpresponse));

			int index = 0;
			while (index < adonisVxp.size()) {
				index = removeDuplicate(adonisVxp, adonisMxp, index);
			}
			adonisVxp.addAll(adonisMxp);
			

			HashMap<Integer, Integer> adoRecords = new HashMap<>();

			for (Adonis vxpValues : adonisVxp) {
				adoRecords.put(vxpValues.getAdonis_PIN(), vxpValues.getAdonis_PIN());
			}

			// Results
			for (Adonis filteredRecord : adonisVxp) {
				if (!adoRecords.containsValue(filteredRecord.getAdonis_PIN())) {
					resultDiff.add(filteredRecord);
				}
				if (!adoRecords.containsValue(filteredRecord.getMxp_PIN())
						&& filteredRecord.isIs_checked_in() == true) {
					resultDiff.add(filteredRecord);
				}
				if (!adoRecords.containsValue(filteredRecord.getVxp_PIN())) {
					resultDiff.add(filteredRecord);
				}
			}

			LOGGER.info("==== RESTful API Response using Spring RESTTemplate START =======");
			LOGGER.info("Size of MXP:" + adonisMxp.size());
			LOGGER.info("Size of VXP:" + adonisVxp.size());
			LOGGER.info("Size of Total Records:" + adonisVxp.size());
			LOGGER.info("Result Diff Records:" + resultDiff.size());
			LOGGER.info("==== RESTful API Response using Spring RESTTemplate END =======");

			model.put("resultDiff", resultDiff);

		} catch (NoSuchElementException e) {
			LOGGER.error("Error" + e.getLocalizedMessage());
			model.put("resultDiff", e.getLocalizedMessage());
			// return new ResponseEntity<List<Adonis>>(HttpStatus.NOT_FOUND);
		}

		return new ModelAndView(viewName, model);
	}
	
	public int removeDuplicate (List<Adonis> adonisVxp, List<Adonis> adonisMxp, int index) {
		for(; index <adonisVxp.size(); index++) {
			
			for (int i=0; i<adonisMxp.size(); i++) {
				if(adonisVxp.get(index).getVxp_PIN().equals(adonisMxp.get(i).getMxp_PIN())) {
					adonisVxp.get(index).setMxp_PIN(adonisMxp.get(i).getMxp_PIN());
					adonisMxp.remove(i);
					return index;
				}
			}
		
		}
		return index;
		
	}
	
	@RequestMapping(value = "/getadonisbydatetime", method = RequestMethod.GET)
	public ResponseEntity<Adonis> getAdonisByDatetime(@RequestParam("pin") Integer id,
			@RequestParam("adonisdate") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date adonisdate,
			@RequestParam("mxpdate") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date mxpdate) {
		Adonis adonis = service.getByDate(id, adonisdate, mxpdate);
		return new ResponseEntity<Adonis>(adonis, HttpStatus.OK);
	}

}
