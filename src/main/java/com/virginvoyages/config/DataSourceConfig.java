package com.virginvoyages.config;

import javax.sql.DataSource;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataSourceConfig {
    
	@Bean
	public DataSource getDataSource() {
		DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
		dataSourceBuilder.driverClassName("com.dremio.jdbc.Driver");
		dataSourceBuilder.url("jdbc:dremio:direct=10.15.13.54:31010");
		dataSourceBuilder.username("chidmon");
		dataSourceBuilder.password("NRYP58pH");
		return dataSourceBuilder.build();
	}
}