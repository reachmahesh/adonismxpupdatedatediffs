package com.virginvoyages.config;

//Run it with:
//javac Main.java && java -cp .:dremio-jdbc-driver-4.2.1-202004111451200819-0c3ecaea.jar Main

//Sample Java Program for Dremio Connectivity
import java.sql.*;
import java.util.Properties;

public class DremioMain {

 public static void main(String[] args) {
     final String DB_URL = "jdbc:dremio:direct=10.15.13.54:31010;";
     final String USER = "chidmon";
     final String PASS = "NRYP58pH";
     Properties props = new Properties();
     props.setProperty("user",USER);
     props.setProperty("password",PASS);

     Connection conn = null;
     Statement stmt = null;
     try {

         System.out.println("Connecting to database...");
         conn = DriverManager.getConnection(DB_URL, props);

         System.out.println("Creating statement...");
         stmt = conn.createStatement();
         String sql;
        // sql = "select user";
         sql = "select * from \"@chidmon\".AdonisMXPupdateDateDiffs";
         ResultSet rs = stmt.executeQuery(sql);

         while (rs.next()) {
        	 System.out.print("Dremio User......: \t"+rs.getInt("Adonis_PIN"));
        	 System.out.println("Adonis TimeStamp"+rs.getTimestamp("Adonis_mod_timestamp"));
        	 System.out.println("MXP TimeStamp"+rs.getTimestamp("MXP_mod_timestamp"));

         }
         rs.close();
         stmt.close();
         conn.close();
     } catch (SQLException se) {
         se.printStackTrace();
     }
 }
}