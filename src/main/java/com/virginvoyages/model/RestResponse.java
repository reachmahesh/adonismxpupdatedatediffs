package com.virginvoyages.model;

import java.util.Arrays;

public class RestResponse {

	private String[] messages;

	private MxpResult result;

	public RestResponse() {
	}

	public String[] getMessages() {
		return messages;
	}

	public void setMessages(String[] messages) {
		this.messages = messages;
	}

	public MxpResult getResult() {
		return result;
	}

	public void setResult(MxpResult result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "RestResponse [messages=" + Arrays.toString(messages) + ", result=" + result + "]";
	}

	
}
