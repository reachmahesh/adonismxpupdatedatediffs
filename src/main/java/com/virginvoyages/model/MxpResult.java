package com.virginvoyages.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MxpResult {
    public int charge_id;
    public String first_name;
    public String middle_name;
    public String last_name;
    public String gender;
    public Object title;
    public String place_of_birth;
    public String date_of_birth;
    public String country_of_birth;
    public String country_of_residence;
    public String country_of_nationality;
    public int marital_status_id;
    public boolean is_person_active;
    public String installation_code;
    public String room_nr;
    public String arrival_date;
    public String departure_date;
    public boolean is_checked_in;
    public int person_id;
    @JsonProperty("PIN") 
    public int pIN;
    public boolean mxp_user;
    public Object username;
    public String position_name;
    public String department_name;
    public int passport_type;
    public String passport_number;
    public String passport_first_name;
    public String passport_last_name;
    public String passport_issued_by;
    public String passport_issued_country;
    public String passport_issued_city;
    public String passport_issued_date;
    public String passport_expiration_date;
    public boolean passport_is_primary;
    public String person_primary_email;
    public String embarkation_city_code;
    public String debarkation_city_code;
	public int getCharge_id() {
		return charge_id;
	}
	public void setCharge_id(int charge_id) {
		this.charge_id = charge_id;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getMiddle_name() {
		return middle_name;
	}
	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Object getTitle() {
		return title;
	}
	public void setTitle(Object title) {
		this.title = title;
	}
	public String getPlace_of_birth() {
		return place_of_birth;
	}
	public void setPlace_of_birth(String place_of_birth) {
		this.place_of_birth = place_of_birth;
	}
	public String getDate_of_birth() {
		return date_of_birth;
	}
	public void setDate_of_birth(String date_of_birth) {
		this.date_of_birth = date_of_birth;
	}
	public String getCountry_of_birth() {
		return country_of_birth;
	}
	public void setCountry_of_birth(String country_of_birth) {
		this.country_of_birth = country_of_birth;
	}
	public String getCountry_of_residence() {
		return country_of_residence;
	}
	public void setCountry_of_residence(String country_of_residence) {
		this.country_of_residence = country_of_residence;
	}
	public String getCountry_of_nationality() {
		return country_of_nationality;
	}
	public void setCountry_of_nationality(String country_of_nationality) {
		this.country_of_nationality = country_of_nationality;
	}
	public int getMarital_status_id() {
		return marital_status_id;
	}
	public void setMarital_status_id(int marital_status_id) {
		this.marital_status_id = marital_status_id;
	}
	public boolean isIs_person_active() {
		return is_person_active;
	}
	public void setIs_person_active(boolean is_person_active) {
		this.is_person_active = is_person_active;
	}
	public String getInstallation_code() {
		return installation_code;
	}
	public void setInstallation_code(String installation_code) {
		this.installation_code = installation_code;
	}
	public String getRoom_nr() {
		return room_nr;
	}
	public void setRoom_nr(String room_nr) {
		this.room_nr = room_nr;
	}
	public String getArrival_date() {
		return arrival_date;
	}
	public void setArrival_date(String arrival_date) {
		this.arrival_date = arrival_date;
	}
	public String getDeparture_date() {
		return departure_date;
	}
	public void setDeparture_date(String departure_date) {
		this.departure_date = departure_date;
	}
	public boolean isIs_checked_in() {
		return is_checked_in;
	}
	public void setIs_checked_in(boolean is_checked_in) {
		this.is_checked_in = is_checked_in;
	}
	public int getPerson_id() {
		return person_id;
	}
	public void setPerson_id(int person_id) {
		this.person_id = person_id;
	}
	public int getpIN() {
		return pIN;
	}
	public void setpIN(int pIN) {
		this.pIN = pIN;
	}
	public boolean isMxp_user() {
		return mxp_user;
	}
	public void setMxp_user(boolean mxp_user) {
		this.mxp_user = mxp_user;
	}
	public Object getUsername() {
		return username;
	}
	public void setUsername(Object username) {
		this.username = username;
	}
	public String getPosition_name() {
		return position_name;
	}
	public void setPosition_name(String position_name) {
		this.position_name = position_name;
	}
	public String getDepartment_name() {
		return department_name;
	}
	public void setDepartment_name(String department_name) {
		this.department_name = department_name;
	}
	public int getPassport_type() {
		return passport_type;
	}
	public void setPassport_type(int passport_type) {
		this.passport_type = passport_type;
	}
	public String getPassport_number() {
		return passport_number;
	}
	public void setPassport_number(String passport_number) {
		this.passport_number = passport_number;
	}
	public String getPassport_first_name() {
		return passport_first_name;
	}
	public void setPassport_first_name(String passport_first_name) {
		this.passport_first_name = passport_first_name;
	}
	public String getPassport_last_name() {
		return passport_last_name;
	}
	public void setPassport_last_name(String passport_last_name) {
		this.passport_last_name = passport_last_name;
	}
	public String getPassport_issued_by() {
		return passport_issued_by;
	}
	public void setPassport_issued_by(String passport_issued_by) {
		this.passport_issued_by = passport_issued_by;
	}
	public String getPassport_issued_country() {
		return passport_issued_country;
	}
	public void setPassport_issued_country(String passport_issued_country) {
		this.passport_issued_country = passport_issued_country;
	}
	public String getPassport_issued_city() {
		return passport_issued_city;
	}
	public void setPassport_issued_city(String passport_issued_city) {
		this.passport_issued_city = passport_issued_city;
	}
	public String getPassport_issued_date() {
		return passport_issued_date;
	}
	public void setPassport_issued_date(String passport_issued_date) {
		this.passport_issued_date = passport_issued_date;
	}
	public String getPassport_expiration_date() {
		return passport_expiration_date;
	}
	public void setPassport_expiration_date(String passport_expiration_date) {
		this.passport_expiration_date = passport_expiration_date;
	}
	public boolean isPassport_is_primary() {
		return passport_is_primary;
	}
	public void setPassport_is_primary(boolean passport_is_primary) {
		this.passport_is_primary = passport_is_primary;
	}
	public String getPerson_primary_email() {
		return person_primary_email;
	}
	public void setPerson_primary_email(String person_primary_email) {
		this.person_primary_email = person_primary_email;
	}
	public String getEmbarkation_city_code() {
		return embarkation_city_code;
	}
	public void setEmbarkation_city_code(String embarkation_city_code) {
		this.embarkation_city_code = embarkation_city_code;
	}
	public String getDebarkation_city_code() {
		return debarkation_city_code;
	}
	public void setDebarkation_city_code(String debarkation_city_code) {
		this.debarkation_city_code = debarkation_city_code;
	}
	@Override
	public String toString() {
		return "Result [charge_id=" + charge_id + ", first_name=" + first_name + ", middle_name=" + middle_name
				+ ", last_name=" + last_name + ", gender=" + gender + ", title=" + title + ", place_of_birth="
				+ place_of_birth + ", date_of_birth=" + date_of_birth + ", country_of_birth=" + country_of_birth
				+ ", country_of_residence=" + country_of_residence + ", country_of_nationality="
				+ country_of_nationality + ", marital_status_id=" + marital_status_id + ", is_person_active="
				+ is_person_active + ", installation_code=" + installation_code + ", room_nr=" + room_nr
				+ ", arrival_date=" + arrival_date + ", departure_date=" + departure_date + ", is_checked_in="
				+ is_checked_in + ", person_id=" + person_id + ", pIN=" + pIN + ", mxp_user=" + mxp_user + ", username="
				+ username + ", position_name=" + position_name + ", department_name=" + department_name
				+ ", passport_type=" + passport_type + ", passport_number=" + passport_number + ", passport_first_name="
				+ passport_first_name + ", passport_last_name=" + passport_last_name + ", passport_issued_by="
				+ passport_issued_by + ", passport_issued_country=" + passport_issued_country
				+ ", passport_issued_city=" + passport_issued_city + ", passport_issued_date=" + passport_issued_date
				+ ", passport_expiration_date=" + passport_expiration_date + ", passport_is_primary="
				+ passport_is_primary + ", person_primary_email=" + person_primary_email + ", embarkation_city_code="
				+ embarkation_city_code + ", debarkation_city_code=" + debarkation_city_code + "]";
	}

	
	
}

