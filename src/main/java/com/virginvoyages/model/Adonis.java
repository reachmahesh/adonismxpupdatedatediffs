package com.virginvoyages.model;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Adonis {

	private Integer adonis_PIN;
	private Integer vxp_PIN;
	private Timestamp adonis_mod_TimeStamp;
	private Timestamp mxp_mod_TimeStamp;
	private Timestamp vxp_mod_TimeStamp;
	private boolean is_checked_in = false;
	private Integer mxp_PIN;

	public Adonis() {
	}


	@Override
	public String toString() {
		return "Adonis [adonis_PIN=" + adonis_PIN + ", vxp_PIN=" + vxp_PIN + ", adonis_mod_TimeStamp="
				+ adonis_mod_TimeStamp + ", mxp_mod_TimeStamp=" + mxp_mod_TimeStamp + ", vxp_mod_TimeStamp="
				+ vxp_mod_TimeStamp + ", is_checked_in=" + is_checked_in + ", mxp_PIN=" + mxp_PIN + "]";
	}



	public Integer getAdonis_PIN() {
		return adonis_PIN;
	}

	public void setAdonis_PIN(Integer adonis_PIN) {
		this.adonis_PIN = adonis_PIN;
	}

	public Integer getVxp_PIN() {
		return vxp_PIN;
	}

	public void setVxp_PIN(Integer adonis_PIN) {
		this.vxp_PIN = adonis_PIN;
	}

	public Timestamp getAdonis_mod_TimeStamp() {
		return adonis_mod_TimeStamp;
	}

	public void setAdonis_mod_TimeStamp(Timestamp adonis_mod_TimeStamp) {
		this.adonis_mod_TimeStamp = adonis_mod_TimeStamp;
	}

	public Timestamp getMxp_mod_TimeStamp() {
		return mxp_mod_TimeStamp;
	}

	public void setMxp_mod_TimeStamp(Timestamp mxp_mod_TimeStamp) {
		this.mxp_mod_TimeStamp = mxp_mod_TimeStamp;
	}

	public Timestamp getVxp_mod_TimeStamp() {
		return vxp_mod_TimeStamp;
	}

	public void setVxp_mod_TimeStamp(Timestamp vxp_mod_TimeStamp) {
		this.vxp_mod_TimeStamp = vxp_mod_TimeStamp;
	}


	public boolean isIs_checked_in() {
		return is_checked_in;
	}


	public void setIs_checked_in(boolean is_checked_in) {
		this.is_checked_in = is_checked_in;
	}

	 @JsonProperty(value = "mxp_PIN", access = JsonProperty.Access.READ_ONLY)
	public Integer getMxp_PIN() {
		return mxp_PIN;
	}

	 @JsonProperty(value = "PIN", access = JsonProperty.Access.WRITE_ONLY)
	public void setMxp_PIN(Integer mxp_PIN) {
		this.mxp_PIN = mxp_PIN;
	}
}
