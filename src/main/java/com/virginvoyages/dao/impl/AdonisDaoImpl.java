package com.virginvoyages.dao.impl;



import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.virginvoyages.dao.AdonisDao;
import com.virginvoyages.model.Adonis;



@Repository
public class AdonisDaoImpl implements AdonisDao {

	private final String FETCH_SQL_BY_ID = "select * from \"@chidmon\".AdonisMXPupdateDateDiffs where Adonis_PIN = :adonis_id";
	
	private static final Logger log = LoggerFactory.getLogger(AdonisDaoImpl.class);
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<Adonis> findMxpByAdonisId(int id) {
		log.info("findMxpByAdonisId query"+"select * from \"@chidmon\".AdonisMXPupdateDateDiffs where adonis_PIN="+id);
		return (List<Adonis>) jdbcTemplate.query( "select * from \"@chidmon\".AdonisMXPupdateDateDiffs where adonis_PIN="+id,new AdonisMxpMapper());
	}
	
	@Override
	public List<Adonis> findAdonisDiff(){
		return (List<Adonis>) jdbcTemplate.query( "select * from \"@chidmon\".AdoVxpCmFull a where a.ado_pin is null or a.vxp_pin is null",new AdonisDiffMapper());
	}

	@Override
	public List<Adonis> findAdonisMxpVxpDiff(){
		return (List<Adonis>) jdbcTemplate.query( "select * from \"@chidmon\".AdoVxpCmFull",new AdonisDiffMapper());
	}
	
	
	@Override
	public List<Adonis> findVxpByAdonisId(int id) {
		/*int result = jdbcTemplate.queryForObject(
			    "SELECT COUNT(*) FROM \"@chidmon\".AdonisMXPupdateDateDiffs", Integer.class);
		log.info("Count..."+result);*/

	/*	SqlParameterSource namedParameters = new MapSqlParameterSource().addValue("id", 200263);
		return (Adonis) namedParameterJdbcTemplate.queryForObject(
		  "select * from \"@chidmon\".AdonisMXPupdateDateDiffs where Adonis_PIN = :id", namedParameters, Adonis.class);*/

		//return (Adonis) jdbcTemplate.queryForObject(FETCH_SQL_BY_ID, new Object[] { id }, new AdonisMapper());
		//Map<String, Object> argMap = new HashMap<String, Object>();
	//	MapSqlParameterSource namedParams = new MapSqlParameterSource("adonis_id", id);
		//argMap.put("adonis_id", id);
		/*return (Adonis) namedParameterJdbcTemplate.queryForObject(
				FETCH_SQL_BY_ID, namedParams, BeanPropertyRowMapper.newInstance(Adonis.class));*/
		log.info("findVxpByAdonisId query"+"select * from \"@chidmon\".AdonisVXPupdateDateDiffs where adonis_PIN="+id);
		return (List<Adonis>) jdbcTemplate.query( "select * from \"@chidmon\".AdonisVXPupdateDateDiffs where adonis_PIN="+id,new AdonisVxpMapper());
	}
	
	@Override
	public Adonis findAdonisByDate (Integer id, Date adonisdate, Date mxpdate) {
		//return (Adonis) jdbcTemplate.queryForObject(FETCH_SQL_BY_DATE, new Object[] { id }, new AdonisMapper());
		return null;
	}


class AdonisMxpMapper implements RowMapper<Adonis> {
	@Override
	public Adonis mapRow(ResultSet rs, int rowNum) throws SQLException {
		Adonis adonis = new Adonis();
		adonis.setAdonis_PIN(rs.getInt("Adonis_PIN"));
		adonis.setAdonis_mod_TimeStamp(rs.getTimestamp("Adonis_mod_timestamp"));
		adonis.setMxp_mod_TimeStamp(rs.getTimestamp("MXP_mod_timestamp"));
		return adonis;
	}
}
	class AdonisVxpMapper implements RowMapper<Adonis> {
		@Override
		public Adonis mapRow(ResultSet rs, int rowNum) throws SQLException {
			Adonis adonis = new Adonis();
			adonis.setAdonis_PIN(rs.getInt("Adonis_PIN"));
			adonis.setAdonis_mod_TimeStamp(rs.getTimestamp("Adonis_mod_timestamp"));
			adonis.setVxp_mod_TimeStamp(rs.getTimestamp("VXP_mod_timestamp"));
			return adonis;
		}
}
	
	class AdonisDiffMapper implements RowMapper<Adonis> {
		@Override
		public Adonis mapRow(ResultSet rs, int rowNum) throws SQLException {
			Adonis adonis = new Adonis();
			adonis.setAdonis_PIN(rs.getInt("ado_pin"));
			adonis.setAdonis_mod_TimeStamp(rs.getTimestamp("ado_mod"));
			adonis.setVxp_PIN(rs.getInt("vxp_pin"));
			adonis.setVxp_mod_TimeStamp(rs.getTimestamp("vxp_mod"));
			return adonis;
		}
}	
}
