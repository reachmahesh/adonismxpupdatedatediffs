package com.virginvoyages.dao;

import java.util.Date;
import java.util.List;

import com.virginvoyages.model.Adonis;

public interface AdonisDao {

	public List<Adonis> findMxpByAdonisId(int id);
	
	public List<Adonis> findVxpByAdonisId(int id);
	
	public Adonis findAdonisByDate (Integer id, Date adonisdate, Date mxpdate);
	
	public List<Adonis> findAdonisDiff();
	
	public List<Adonis> findAdonisMxpVxpDiff();
}
